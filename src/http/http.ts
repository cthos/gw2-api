import axios from "axios";

export interface ICoreHttp {
  callAPI(url: URL, options?: any): Promise<any>;
}

export class CoreHttp implements ICoreHttp {
  /**
   * Calls the GW2 API.
   *
   * @param url The URL to make the axios request to
   * @param options The options to make the axios request with
   */
  public async callAPI(url: URL, options: any = {}): Promise<any> {
    try {
      const response = await axios(url.toString(), options);

      return response.data;
    } catch (error) {
      if (axios.isAxiosError(error) && error.response) {
        throw new Error(`The API Returned an error: ${error.response.status}`);
      }

      throw error;
    }
  }
}
