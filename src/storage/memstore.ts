export class Memstore {
  protected cache: any = {};
  
  public async setItem(key: string, val: string): Promise<Memstore> {
    this.cache[key] = val;
    return this;
  }

  public async getItem(key: string): Promise<any> {
    return this.cache[key];
  }
}