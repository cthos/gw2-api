import * as _ from "underscore";
import * as md5 from "md5";
import * as req from "request";
import * as chunk from "chunk";
import { Account } from "./endpoints/account/account";
import { Achievements } from "./endpoints/achievements/achievements";
import { CoreHttp, ICoreHttp } from "../http/http";
import { Characters } from "./endpoints/characters/characters";
import { Inventory } from "./endpoints/inventory/inventory";
import { Game } from "./game/game";
import { WizardsVaultResult } from "./endpoints/account/wizards_vault.types";

export class GW2API {
  protected baseUrl = "https://api.guildwars2.com/v2/";
  protected storage: any;
  protected useAuthHeader = true;
  protected lang = "en";
  protected cache = true;
  protected storeInCache = true;

  public account: Account;
  public achievements: Achievements;
  public characters: Characters;
  public inventory: Inventory;
  public game: Game;

  protected static singleton;

  constructor(private http?: ICoreHttp) {
    if (!this.http) {
      this.http = new CoreHttp();
    }

    this.account = new Account(this);
    this.achievements = new Achievements(this);
    this.characters = new Characters(this);
    this.inventory = new Inventory(this);
    this.game = new Game(this);
  }

  public static getInstance(): GW2API {
    if (!GW2API.singleton) {
      GW2API.singleton = new GW2API();
    }

    return GW2API.singleton;
  }

  /**
   * Set the storage solution.
   *
   * The solution should have a getItem and setItem method, but can be anything.
   *
   * @param {object} storage
   *  Storage solution. Defaults to localStorage if available. Null if not.
   */
  public setStorage(storage: any): GW2API {
    this.storage = storage;

    return this;
  }

  /**
   * Gets the storage solution.
   *
   * @return {object}
   *   Storage solution.
   */
  public getStorage(): any {
    return this.storage;
  }

  /**
   * Setter for the useAuthHeader property.
   *
   * Typically you'll set this to false if you're in a browser
   * because the API doesn't support OPTIONS.
   *
   * @param {boolean} useAuthHeader
   * @returns {GW2API}
   */
  public setUseAuthHeader(useAuth: boolean): GW2API {
    this.useAuthHeader = useAuth;

    return this;
  }

  /**
   * Getter for useAuthHeader.
   *
   * @returns {boolean}
   */
  public getUseAuthHeader(): boolean {
    return this.useAuthHeader;
  }

  /**
   * Sets the language code. Should be the ISO code (en_US for example).
   *
   * @param {string} langCode
   *   Target language code.
   *
   * @return this
   */
  public setLang(langCode: string): GW2API {
    this.lang = langCode;
    return this;
  }

  /**
   * Gets the current language, which some api endpoints use.
   *
   * @return {string}
   *  langcode
   */
  public getLang(): string {
    return this.lang;
  }

  /**
   * Gets the boolean cache setting.
   *
   * @return {boolean}
   */
  public getCache(): boolean {
    return this.cache;
  }

  /**
   * Turns caching on or off.
   *
   * @param {boolean} cache
   *   Enable or disable cache.
   *
   * @return this
   */
  setCache(cache: boolean): GW2API {
    this.cache = this.storeInCache = cache;
    return this;
  }

  /**
   * Enables or disables storing API results in cache.
   * This is distinct from setCache, which turns all caching on or off.
   *
   * This setting would be used to update the cache but not actually return
   * the results.
   *
   * @param {boolean} storeInCache
   *   Whether or not to store results in cache.
   *
   * @returns this
   */
  public setStoreInCache(storeInCache: boolean): GW2API {
    this.storeInCache = storeInCache;
    return this;
  }

  /**
   * Stores the API key in storage.
   *
   * @param {string} key
   *   API Key to use for requests.
   * @return Promise<GW2API>
   */
  public async setAPIKey(key: string): Promise<GW2API> {
    await this.storage.setItem("apiKey", key);
    return this;
  }

  /**
   * Loads the API key from the local storage.
   *
   * @return Promise<string>
   */
  public async getAPIKey(): Promise<string> {
    return this.storage.getItem("apiKey");
  }

  /**
   * Helper method to convert a list of ids (or objects with an id parameter)
   * into more useful information via the passed endpointFunc.
   *
   * It chunks the array into chunkSize pieces and makes that many api calls
   * in parallel.
   *
   * Usually used for the account calls that don't return full details on equipment,
   * for example.
   *
   * @param  {function} endpointFunc
   *   The function to call to get more details. Must be in the GW2API object
   *   and must return a promise.
   * @param  {Array} Items
   *   An array of items to transform. Either an array of ints or objects.
   * @param  {Int} chunkSize
   *   How large each batch call should be. Defaults to 100.
   *
   * @return {Promise}
   */
  public getDeeperInfo(
    endpointFunc: Function,
    items: [any],
    chunkSize = 100,
    context: any = null,
  ): Promise<Array<any>> {
    const lookupIds = [];
    const promises = [];

    if (!chunkSize) {
      chunkSize = 100;
    }

    items.forEach((item) => {
      if (!item) {
        // Some endpoints return null, for empty bank slots, for example.
        return;
      }
      if (typeof item == "number") {
        lookupIds.push(item);
        return;
      }
      lookupIds.push(item.id);
    });

    const ctx = context ? context : this;

    const chunks = chunk(lookupIds, chunkSize);
    chunks.forEach((ch) => {
      promises.push(endpointFunc.call(ctx, ch));
    });

    return Promise.all(promises).then(function (results) {
      const reses = [].concat(...results);

      reses.forEach((res) => {
        for (let i = 0, len = items.length; i < len; i++) {
          if (!items[i]) {
            continue;
          }

          if (typeof items[i] == "number") {
            items[i] = { id: items[i] };
          }

          if (items[i].id === res.id) {
            Object.assign(items[i], res);
          }
        }
      });

      return items;
    });
  }

  /**
   * Helper function to do the common endpoint/{id} or ?ids={}
   *
   * @param string endpoint
   * @param mixed ids
   * @param boolean requiresAuth
   *
   * @return Promise
   */
  public getOneOrMany(
    endpoint: string,
    ids?: string | string[] | number | number[],
    requiresAuth = true,
    otherParams?: any,
  ): Promise<any> {
    const params = {};

    if (typeof ids === "number" || typeof ids === "string") {
      endpoint += "/" + ids;
    } else if (Array.isArray(ids)) {
      params["ids"] = ids.sort().join(",");
    }

    if (typeof otherParams === "object") {
      Object.assign(params, otherParams);
    }

    return this.callAPI(endpoint, params, requiresAuth);
  }

  /**
   * Makes a call to the GW2 API.
   *
   * @param endpoint
   * @param params
   * @param requiresAuth
   *
   * @return Promise
   */
  public async callAPI(endpoint: string, params?: any, requiresAuth: any = true): Promise<any> {
    if (typeof requiresAuth == "undefined") {
      requiresAuth = true;
    }

    if (!params) {
      params = {};
    }

    if (endpoint.indexOf("/") === 0) {
      endpoint = endpoint.substring(1);
    }

    const url = new URL(`${this.baseUrl}${endpoint}`);
    const options = {};

    if (requiresAuth) {
      if (this.useAuthHeader) {
        options["headers"] = {
          Authorization: "Bearer " + (await this.getAPIKey()),
        };
      } else {
        params["access_token"] = await this.getAPIKey();
      }
    }

    if (!this.cache) {
      // TODO: Appending a cache-busting string to the end, because GW2 API does not support CORS
      params["r"] = new Date().getTime();
    }

    for (const key in params) {
      url.searchParams.append(key, params[key]);
    }

    const keystr: string = url.searchParams.toString();
    const cacheKey: string = md5(endpoint + (keystr ? "?" + keystr : ""));
    let cachedItem: any;

    console.log(this.cache, options);

    if (this.cache && (cachedItem = await this.storage.getItem(cacheKey))) {
      cachedItem = JSON.parse(cachedItem);
      return cachedItem;
    }

    return this.http.callAPI(url, options).then(async (data) => {
      if (this.storeInCache) {
        await this.storage.setItem(cacheKey, JSON.stringify(data));
      }

      return data;
    });
  }

  // MARK: External API Functions
  /**
   * Gets account information.
   *
   * @returns {Promise}
   */
  public getAccount(): Promise<any> {
    return this.account.get();
  }

  /**
   * @see Characters.get
   */
  public async getCharacters(characterName?: string): Promise<any> {
    return this.characters.get(characterName);
  }

  /**
   * @see Achievements.getByAccount
   */
  public async getAccountAchievements(autoTranslateAchievements: boolean): Promise<any> {
    return this.achievements.getByAccount(autoTranslateAchievements);
  }

  /**
   * @see Achievements.get
   */
  public async getAchievements(achievementIds?: number[] | number): Promise<WizardsVaultResult> {
    return this.achievements.get(achievementIds);
  }

  /**
   * @see Inventory.getItems
   */
  public async getItems(itemIds: number | number[]): Promise<any> {
    return this.inventory.getItems(itemIds);
  }

  /**
   * @see Account.getBank
   */
  public async getAccountBank(autoTranslateItems: boolean): Promise<any> {
    return this.account.getBank(autoTranslateItems);
  }

  /**
   * @see Account.getDyes
   */
  public async getAccountDyes(autoTranslateItems = false): Promise<any> {
    return this.account.getDyes(autoTranslateItems);
  }

  /**
   * @see Inventory.getMaterials
   */
  public async getAccountMaterials(autoTranslateItems = false): Promise<any> {
    return this.inventory.getMaterials(autoTranslateItems);
  }

  /**
   * @see Account.getMasteries
   */
  public async getAccountMasteries(autoTranslateMasteries = false): Promise<any> {
    return this.account.getMasteries(autoTranslateMasteries);
  }

  /**
   * @see Account.getFinishers
   */
  public async getAccountFinishers(autoTranslate = false): Promise<any> {
    return this.account.getFinishers(autoTranslate);
  }

  /**
   * @see Account.getMinis
   */
  public async getAccountMinis(autoTranslateItems = false): Promise<any> {
    return this.account.getMinis(autoTranslateItems);
  }

  /**
   * @see Inventory.getSkins
   */
  public async getAccountSkins(autoTranslateItems: boolean): Promise<any> {
    return this.inventory.getSkins(autoTranslateItems);
  }

  /**
   * Gets an account's world boss progression for this reset period.
   *
   * @param  {Boolean} autoTranslateItems
   *   <optional> If passed as true, will automatically get item descriptions
   *   from the items api.
   * @return {Promise}
   */
  public async getAccountWorldBosses(autoTranslateItems = true): Promise<any> {
    return this.account.getWorldBosses(autoTranslateItems);
  }

  /**
   * @see Game.getWorldBosses
   */
  public async getWorldBosses(ids?: number[] | number): Promise<any> {
    return this.game.getWorldBosses(ids);
  }

  /**
   * Gets an account's commerce transactions.
   *
   * @param {Boolean} current
   *   If true, this will query current transactions. Otherwise it
   *   will query historical transactions.
   * @param {String} secondLevel
   *   Either "buys" or "Sells"
   * @return {Promise}
   */
  public async getCommerceTransactions(current: boolean, secondLevel: string): Promise<boolean> {
    return this.account.getCommerceTransactions(current, secondLevel);
  }

  /**
   * @see Game.getCommerceListings
   */
  public async getCommerceListings(itemIds: number[] | number): Promise<any> {
    return this.game.getCommerceListings(itemIds);
  }

  /**
   * @see Game.getCommerceExchange
   */
  public async getCommerceExchange(gemOrCoin: "gems" | "coin", quantity: number): Promise<any> {
    return this.game.getCommerceExchange(gemOrCoin, quantity);
  }

  /**
   * Gets overall account pvp statistics.
   *
   * @return {Promise}
   */
  public async getPVPStats(): Promise<any> {
    return this.account.getPVPStats();
  }

  /**
   * @see Account.getPVPGames
   */
  public async getPVPGames(gameIds?: string[] | string): Promise<any> {
    return this.account.getPVPGames(gameIds);
  }

  /**
   * @see Account.getWVWMatches
   */
  public async getWVWMatches(worldId: number, matchIds: number[] | number): Promise<any> {
    return this.account.getWVWMatches(worldId, matchIds);
  }

  /**
   * Gets WVW Objectives
   *
   * @param {String|Array} objectiveIds
   *   <optional> Either an objectiveId or array of ids.
   *
   */
  public async getWVWObjectives(objectiveIds?: string[] | string): Promise<any> {
    return this.getOneOrMany("/wvw/objectives", objectiveIds);
  }

  /**
   * Returns info about a given token. This token must be first set via
   * this.setAPIKey.
   *
   * @return {Promise}
   */
  public async getTokenInfo(): Promise<any> {
    return this.callAPI("/tokeninfo");
  }

  /**
   * @see Account.getWallet
   */
  public async getWallet(handleCurrencyTranslation: boolean): Promise<any> {
    return this.account.getWallet(handleCurrencyTranslation);
  }

  /**
   * @see Game.getMasteries
   */
  public async getMasteries(masteryIds?: number[] | number): Promise<any> {
    return this.game.getMasteries(masteryIds);
  }

  /**
   * @see Game.getFinishers
   */
  public async getFinishers(finisherIds?: number[] | number): Promise<any> {
    return this.game.getFinishers(finisherIds);
  }

  /**
   * Gets Dye Colors. If no ids are passed, all possible ids are returned.
   *
   * @param  {int|Array} colorIds
   *   <optional> An int or array of color ids.
   * @return {Promise}
   */
  public async getColors(colorIds?: number[] | number): Promise<any> {
    return this.inventory.getColors(colorIds);
  }

  /**
   * Returns the continents list
   * @return Promise
   */
  public async getContinents(): Promise<any> {
    return this.callAPI("/continents", null, false);
  }

  /**
   * Returns commonly requested files.
   *
   * @param {String|Array} fileIds
   *  Either a string file id or an array of ids.
   *
   * @return {Promise}
   */
  public async getFiles(fileIds?: string[] | string): Promise<any> {
    return this.getOneOrMany("/files", fileIds, false);
  }

  /**
   * Returns the current build id.
   *
   * @return {Promise}
   */
  public async getBuildId(): Promise<any> {
    return this.callAPI("/build");
  }

  /**
   * Returns a list of Quaggans!
   *
   * @param {String|Array} quagganIds
   *   <optional> a String quaggan id or an array of quaggan ids.
   *
   * @return {Promise}
   */
  public async getQuaggans(quagganIds?: string[] | string): Promise<any> {
    return this.getOneOrMany("/quaggans", quagganIds, false);
  }

  /**
   * Gets materials. If no ids are passed, this will return an array of all
   * possible material ids.
   * @param  {int|array} materialIds
   *   <optional> Either an int materialId or an array of materialIds
   * @return Promise
   */
  public async getMaterials(materialIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/materials", materialIds, false);
  }

  /**
   * Gets minis. If no ids are passed, this will return an array of all
   * possible mini ids.
   * @param  {Int|Array}  miniIds
   *   <optional> Either an int or an array of mini ids.
   * @return {Promise}
   */
  public async getMinis(miniIds?: number[] | number): Promise<any> {
    return this.inventory.getMinis(miniIds);
  }

  /**
   * Gets recipes. If no ids are passed, this will return an array of all
   * possible recipe ids.
   * @param  {int|array} recipeIds
   *   <optional> Either an int recipeId or an array of recipeIds
   * @return Promise
   */
  public async getRecipes(recipeIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/recipes", recipeIds, false);
  }

  /**
   * Searches for recipes which match an item id. inputItem and outputItem
   * are mutually exclusive.
   *
   * @param  {Int} inputItem
   *   Search for recipes containing this item.
   * @param  {Int} outputItem
   *   Search for recipes which will produce this item.
   * @return {Promise}
   */
  public async searchRecipes(inputItem: number, outputItem?: number): Promise<any> {
    if (inputItem && outputItem) {
      return new Promise((_fulfill, reject) => {
        reject("inputItem and outputItem are mutually exclusive options");
      });
    }

    const options = _.omit({ input: inputItem, output: outputItem }, (v, _k) => {
      if (!v) {
        return true;
      }
    });

    return this.callAPI("/recipes/search", options, false);
  }

  /**
   * Gets Skins. If no ids are passed, this returns an array of all skins.
   *
   * @param  {Int|Array} skinIds
   *   <optional> Either an int skinId or an array of skin ids
   * @return {Promise}
   */
  public async getSkins(skinIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/skins", skinIds, false);
  }

  /**
   * @see Game.getCurrencies
   */
  public async getCurrencies(currencyIds?: number[] | number): Promise<any> {
    return this.game.getCurrencies(currencyIds);
  }

  /**
   * Gets achievement groups. Examples being "Heart of Thorns, Central Tyria"
   *
   * @param {String|Array} groupIds
   *  <optional> Either a groupId or array of group ids. Note that for this, ids
   *  are guids.
   *
   * @return {Promise}
   */
  public async getAchievementGroups(groupIds?: string[] | string): Promise<any> {
    return this.getOneOrMany("achievements/groups", groupIds, false);
  }

  /**
   * Gets achievement categories. Examples being "Slayer, Hero of Tyria"
   *
   * @param {Int|Array} categoryIds
   *  <optional> Either an int or an array of category ids.
   *
   * @return {Promise}
   */
  public async getAchievementCategories(categoryIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("achievements/categories", categoryIds, false);
  }

  /**
   * Gets the daily achievements.
   *
   * @returns {Promise<WizardsVaultResult>}
   */
  public async getDailyAchievements(): Promise<WizardsVaultResult> {
    return this.account.getWizardVaultDailyAchievements();
  }

  /**
   * Gets the daily achievements.
   *
   * @returns {Promise<WizardsVaultResult>}
   */
  public async getWeeklyAchievements(): Promise<WizardsVaultResult> {
    return this.account.getWizardVaultWeeklyAchievements();
  }

  /**
   * Gets skills. If no ids are passed, this will return an array of all possible
   * skills.
   *
   * @param  {int|array} skillIds
   *   <optional> Either an int skillId or an array of skillIds.
   * @return {Promise}
   */
  public async getSkills(skillIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/skills", skillIds, false);
  }

  /**
   * [Helper Method] Gets skills for a particular profession.
   *
   * @param  {String} profession
   *   The string key to match profession on.
   * @param {String} skillType
   *   <optional> The type of skills to return ("Weapon", "Heal", etc.)
   * @param {Boolean} includeBundles
   *   <optional> Whether or not to include bundles as part of the return list.
   *   This option is meaningless if skillType == 'Bundle'
   * @return {Promise}
   */
  public async getProfessionSkills(
    profession: string,
    skillType: string,
    includeBundles: boolean,
  ): Promise<any> {
    const that = this;

    if (typeof includeBundles == "undefined") {
      includeBundles = false;
    }

    return this.getSkills().then((skillIds) => {
      // Break skills into chunks.
      const chunks = chunk(skillIds, 50);
      const promises = [];

      chunks.forEach((c) => {
        promises.push(
          that.getSkills(c).then((skills) => {
            return skills.filter((skill) => {
              if (!skill.professions) {
                return false;
              }

              if (skill.professions.indexOf(profession) == -1) {
                return false;
              }

              if (!includeBundles && skill.type == "Bundle") {
                return false;
              }

              if (skillType && skill.type == skillType) {
                return true;
              } else if (skillType) {
                return false;
              }

              return true;
            });
          }),
        );
      });

      return Promise.all(promises).then((results) => {
        return [].concat(...results);
      });
    });
  }

  /**
   * Gets Specializations. If no ids are passed this will return an array of all
   * ids.
   *
   * @param  {Int|Array} specializationIds
   *   <optional> Either an int specialization id or an array of them.
   * @return {Promise}
   */
  public async getSpecializations(specializationIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/specializations", specializationIds, false);
  }

  /**
   * Gets a list of profession specializations.
   *
   * @param  {String} profession
   *   Profession name. Remember to uppercase the first letter.
   *
   * @return {Promise}
   */
  public async getProfessionSpecializations(profession: string) {
    const that = this;
    return this.getSpecializations()
      .then((specializationIds) => {
        // Doing this for the inherant chunking.
        return this.getDeeperInfo(that.getSpecializations, specializationIds);
      })
      .then((fullSpecializations) => {
        const specs = [];
        fullSpecializations.forEach((spec) => {
          if (spec.profession !== profession) {
            return;
          }
          specs.push(spec);
        });

        return specs;
      });
  }

  /**
   * Gets a list of traits from the passed ids. If no traitIds are passed
   * all trait ids are returned.
   *
   * @param  {Int|Array} traitIds
   *   <optional> An int or array of trait ids.
   *
   * @return {Promise}
   */
  public async getTraits(traitIds?: number[] | number): Promise<any> {
    return this.getOneOrMany("/traits", traitIds, false);
  }

  /**
   * Returns the assets required to render emblems.
   *
   * @param  {String} foreOrBack
   *   Either the string "foregrounds" or "backgrounds"
   * @param  {Int|Array} assetIds
   *   <optional> Either an Int or Array assetId
   *
   * @return {Promise}
   */
  public async getEmblems(
    foreOrBack: "foregrounds" | "backgrounds",
    assetIds?: number[] | number,
  ): Promise<any> {
    const subpoint = foreOrBack === "foregrounds" ? "foregrounds" : "backgrounds";
    return this.getOneOrMany("/emblem/" + subpoint, assetIds);
  }

  /**
   * Gets info about guild permissions (unauthenticated).
   *
   * @param  {String|Array} permissionIds
   *
   * @return {Promise}
   */
  public async getGuildPermissions(permissionIds: string[] | string): Promise<any> {
    return this.getOneOrMany("/guild/permissions", permissionIds);
  }

  /**
   * Gets info about guild upgrades (unauthenticated).
   *
   * @param  {Int|Array} upgradeIds
   *   <optional> Either an int or an array of upgrade ids.
   *
   * @return {Promise}
   */
  public async getGuildUpgrades(upgradeIds: number[] | number): Promise<any> {
    return this.getOneOrMany("/guild/upgrades", upgradeIds);
  }
}
