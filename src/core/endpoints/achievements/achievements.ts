import { Endpoint } from "../endpoint";

export class Achievements extends Endpoint {
    /**
     * Gets achievements. If no ids are passed, this will return an array of all
     * possible achievement ids.
     * @param  {int|array} achievementIds
     *   <optional> Either an int achievementId or an array of achievementIds
     * @return Promise
     */
    public async get(achievementIds?: number | number[]): Promise<any> {
        return this.core.getOneOrMany('/achievements', achievementIds, false, { "lang": this.core.getLang() });
    }

    /**
     * Gets Account achievements.
     *
     * @param {Boolean} autoTranslateAchievements
     *   If this is set to true, it will automatically call the achievement
     *   endpoint to get more details as part of the return.
     *
     * @return {Array}
     *   Account achievements.
     */
    public async getByAccount(autoTranslateAchievements?: boolean): Promise<any> {
        const p = this.core.callAPI('/account/achievements');

        if (!autoTranslateAchievements) {
            return p;
        }

        return p.then((accountAchievements) => {
            return this.core.getDeeperInfo(this.get, accountAchievements, 100, this);
        });
    }
}