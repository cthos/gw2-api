import { Achievements } from "./achievements";
import { CoreHttp } from "../../../http/http";
import { Memstore } from "../../../storage/memstore";
import { GW2API } from "../../main";

jest.mock('../../../http/http', () => {
    return {
        CoreHttp: jest.fn().mockImplementation(() => {
            return {
                callAPI: jest.fn().mockResolvedValue({success: true}),
            }
        })
    }
});

describe('Achievements', () => {
    let achievements;
    let http;
    let api;

    beforeEach(() => {
        const memstore = new Memstore();
        http = new CoreHttp();
        api = new GW2API(http);
        api.setStorage(memstore);

        achievements = new Achievements(api);
    });

    it('should get achievements', async () => {
        const res = await achievements.get();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/achievements?lang=en');

        expect(res.success).toBeTruthy();
    });

    it('should get achievements by id', async () => {
        const res = await achievements.get([1, 22]);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/achievements?ids=1%2C22&lang=en');

        expect(res.success).toBeTruthy();
    });
})