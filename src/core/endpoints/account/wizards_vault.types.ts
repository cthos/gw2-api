export interface WizardsVaultObjective {
  id: number;
  title: string;
  track: string;
  acclaim: number;
  progress_current: number;
  progress_complete: number;
  claimed: boolean;
}

export interface WizardsVaultResult {
  meta_progress_current: number;
  meta_progress_complete: number;
  meta_reward_item_id: number;
  meta_reward_item?: any;
  meta_reward_astral: number;
  meta_reward_claimed: boolean;
  objectives: WizardsVaultObjective[];
}
