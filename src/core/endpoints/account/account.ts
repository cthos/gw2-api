import { Endpoint } from "../endpoint";
import { WizardsVaultResult } from "./wizards_vault.types";

export class Account extends Endpoint {
  public async get(): Promise<any> {
    const endpoint = "/account";

    return this.core.callAPI(endpoint);
  }

  /**
   * Gets items from the account bank.
   *
   * @param  {Boolean} autoTranslateItems
   *   Whether or not to automatically call the item endpoint.
   * @return {Promise}
   */
  public async getBank(autoTranslateItems = false): Promise<any> {
    const p = this.core.callAPI("/account/bank");

    if (!autoTranslateItems) {
      return p;
    }

    return p.then((bank) => {
      return this.core.getDeeperInfo(this.core.inventory.getItems, bank, 100, this.core.inventory);
    });
  }

  /**
   * Gets the account's masteries.
   *
   * @param {Boolean} autoTranslateMasteries
   *
   * @returns {Promise<any>}
   */
  public async getMasteries(autoTranslateMasteries = false): Promise<any> {
    const p = this.core.callAPI("/account/masteries");

    if (!autoTranslateMasteries) {
      return p;
    }

    return p.then((masteries) => {
      return this.core.getDeeperInfo(this.core.game.getMasteries, masteries, 100, this.core.game);
    });
  }

  /**
   * Gets unlocked account dyes.
   * @param  {Boolean} autoTranslateItems
   *   <optional> If passed as true, will automatically get item descriptions
   *   from the items api.
   * @return {Promise}
   */
  public async getDyes(autoTranslateItems = false): Promise<any> {
    const p = this.core.callAPI("/account/dyes");

    if (!autoTranslateItems) {
      return p;
    }

    return p.then((dyes) => {
      return this.core.getDeeperInfo(this.core.inventory.getColors, dyes, 100, this.core.inventory);
    });
  }

  /**
   * Gets the account's unlocked minis.
   *
   * @param  {Boolean} autoTranslateItems
   *   <optional> If passed as true, will automatically get item descriptions
   *   from the items api.
   * @return {Promise}
   */
  public async getMinis(autoTranslateItems = false): Promise<any> {
    const p = this.core.callAPI("/account/minis");

    if (!autoTranslateItems) {
      return p;
    }

    return p.then((minis) => {
      return this.core.getDeeperInfo(this.core.inventory.getMinis, minis, 100, this.core.inventory);
    });
  }

  public async getWorldBosses(autoTranslateItems = false): Promise<any> {
    const p = this.core.callAPI("/account/worldbosses");

    if (!autoTranslateItems) {
      return p;
    }

    return p.then((bosses) => {
      return this.core.getDeeperInfo(this.core.game.getWorldBosses, bosses, 100);
    });
  }

  /**
   * Gets an account's commerce transactions.
   *
   * @param {Boolean} current
   *   If true, this will query current transactions. Otherwise it
   *   will query historical transactions.
   * @param {String} secondLevel
   *   Either "buys" or "Sells"
   * @return {Promise}
   */
  public async getCommerceTransactions(current: boolean, secondLevel: string): Promise<boolean> {
    const endpoint =
      "/commerce/transactions/" + (current ? "current" : "history") + "/" + secondLevel;
    return this.core.callAPI(endpoint);
  }

  /**
   * Gets overall account pvp statistics.
   *
   * @return {Promise}
   */
  public async getPVPStats(): Promise<any> {
    return this.core.callAPI("/pvp/stats");
  }

  /**
   * Gets PVP Game details. If ids are not passed a list of all game ids
   * are returned.
   *
   * @param  {String|Array} gameIds
   *   <optional> Either a gameId or an array of games you'd like more details
   *   on. Note that GameId is a uuid.
   * @return {Promise}
   */
  public async getPVPGames(gameIds?: string[] | string): Promise<any> {
    return this.core.getOneOrMany("/pvp/games", gameIds);
  }

  /**
   * Gets WVW Matches.
   *
   * @param {Int} worldId
   *   A world who's id is participating in the match.
   * @param  {String|Array} matchIds
   *   String match id, or an array of match ids.
   *
   * @return {Promise}
   */
  public async getWVWMatches(worldId: number, matchIds: number[] | number): Promise<any> {
    return this.core.getOneOrMany("/wvw/matches", matchIds, false, { world: worldId });
  }

  /**
   * Gets the account's finishers.
   *
   * @param {Boolean} autoTranslate
   *
   * @returns {Promise}
   */
  public async getFinishers(autoTranslate = false): Promise<any> {
    const p = this.core.callAPI("/account/finishers");

    if (!autoTranslate) {
      return p;
    }

    return p.then((finishers) => {
      return this.core.getDeeperInfo(this.core.game.getFinishers, finishers, 100, this.core.game);
    });
  }

  public async getWallet(handleCurrencyTranslation: boolean): Promise<any> {
    if (!handleCurrencyTranslation) {
      return this.core.callAPI("/account/wallet");
    }

    return this.core.callAPI("/account/wallet").then((res) => {
      const walletCurrencies = res;
      const lookupIds: number[] = [];
      for (let i = 0, len = res.length; i < len; i++) {
        lookupIds.push(res[i].id as number);
      }

      return this.core.game.getCurrencies(lookupIds).then((res) => {
        for (let i = 0, len = res.length; i < len; i++) {
          for (let x = 0, xlen = walletCurrencies.length; x < xlen; x++) {
            if (res[i].id == walletCurrencies[x].id) {
              Object.assign(walletCurrencies[x], res[i]);
              break;
            }
          }
        }
        return walletCurrencies;
      });
    });
  }

  public async getWizardVaultDailyAchievements(): Promise<WizardsVaultResult> {
    return this.core.callAPI("/account/wizardsvault/daily");
  }

  public async getWizardVaultWeeklyAchievements(): Promise<WizardsVaultResult> {
    return this.core.callAPI("/account/wizardsvault/weekly");
  }
}
