import { CoreHttp } from "../../../http/http";
import { Memstore } from "../../../storage/memstore";
import { GW2API } from "../../main";
import { Account } from "./account";
import { HttpMock } from "../../testing/mock";

const mock = new HttpMock();

jest.mock('../../../http/http', () => {
    return {
        CoreHttp: jest.fn().mockImplementation(() => {
            return {
                callAPI: mock.getCallApiMock(),
            }
        })
    }
});

describe('Account', () => {
    let account;
    let http;
    let api;

    beforeEach(() => {
        const memstore = new Memstore();
        http = new CoreHttp();
        api = new GW2API(http);
        api.setStorage(memstore);

        account = new Account(api);
    });

    it('should get an account', async () => {
        const res = await account.get();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account');

        expect(res.success).toEqual(true);
    });

    it('should get masteries', async () => {
        const res = await account.getMasteries();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/masteries');

        expect(res[0].id).toEqual(1);
    });

    it('should get masteries deeply', async () => {
        const res = await account.getMasteries(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/masteries');

        expect(res[0].name).toEqual('Exalted Lore');
    });

    it('should get the account bank', async () => {
        const res = await account.getBank();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/bank');

        expect(res[0].id).toEqual(19675);
    });

    it('should get the account bank with translate items', async () => {
        const res = await account.getBank(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/bank');

        expect(res[0].id).toEqual(19675);
        expect(res[0].name).toEqual("Testberry Bar");
    });

    it('should get the account dyes', async () => {
        const res = await account.getDyes();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/dyes');

        expect(res[0]).toEqual(3);
    });

    it('should get the account dyes deeply', async () => {
        const res = await account.getDyes(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/dyes');

        expect(res[0].name).toEqual('Sky');
    });

    it('should get the account minis', async () => {
        const res = await account.getMinis();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/minis');

        expect(res[0]).toEqual(1);
    });

    it('should get the account minis deeply', async () => {
        const res = await account.getMinis(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/minis');

        expect(res[0].name).toEqual('Mini-Rytlock');
    });

    it('should get the account wallet', async () => {
        const res = await account.getWallet();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/wallet');

        expect(res[0].id).toEqual(1);
    });

    it('should get the account wallet translating currencies', async () => {
        const res = await account.getWallet(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/wallet');

        expect(res[0].name).toEqual('Coin');
    });

    it('should get account finishers', async () => {
        const res = await account.getFinishers();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/finishers');

        expect(res[0].id).toEqual(1);
        expect(res[0].permanent).toEqual(true);
    });

    it('should get account finishers deeply', async () => {
        const res = await account.getFinishers(true);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/account/finishers');

        expect(res[0].name).toEqual('Rabbit Rank Finisher');
        expect(res[0].order).toEqual(18);
    });
})