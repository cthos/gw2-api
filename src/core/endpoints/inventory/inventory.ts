import { Endpoint } from "../endpoint";

export class Inventory extends Endpoint {
  /**
   * Gets a list of items. If no ids are passed, you'll get an array of all ids back.
   *
   * @param  {int|array} itemIds
   *   <optional> Either an int itemId or an array of itemIds.
   *
   * @return Promise
   */
  public async getItems(ids?: number | number[]): Promise<any> {
    return this.core.getOneOrMany("/items", ids, true);
  }

  public async getColors(ids: number | number[]): Promise<any> {
    return this.core.getOneOrMany("/colors", ids, false);
  }

  /**
   * Gets minis. If no ids are passed, this will return an array of all
   * possible mini ids.
   * @param  {Int|Array}  miniIds
   *   <optional> Either an int or an array of mini ids.
   * @return {Promise}
   */
  public async getMinis(miniIds?: number[] | number): Promise<any> {
    return this.core.getOneOrMany("/minis", miniIds, false);
  }

  /**
   * Gets the account's item skins.
   *
   * @param  {Boolean} autoTranslateItems
   *   <optional> If passed as true, will automatically get item descriptions
   *   from the items api.
   * @return {Promise}
   */
  public async getSkins(autoTranslateItems = false): Promise<any> {
    const skins = await this.core.callAPI("/account/skins");

    if (!autoTranslateItems) {
      return skins;
    }

    return this.core.getDeeperInfo(this.getSkins, skins, 100, this);
  }

  /**
   * Gets the account's material storage.
   *
   * @param  {Boolean} autoTranslateItems
   *   <optional> If passed as true, will automatically get item descriptions
   *   from the materials api.
   * @return {Promise}
   */
  public async getMaterials(autoTranslateItems = false): Promise<any> {
    const materials = await this.core.callAPI("/account/materials");

    if (!autoTranslateItems) {
      return materials;
    }

    return this.core.getDeeperInfo(this.getItems, materials, 100, this);
  }
}
