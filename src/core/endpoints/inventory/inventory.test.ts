import { Inventory } from "./inventory";
import { CoreHttp } from "../../../http/http";
import { Memstore } from "../../../storage/memstore";
import { GW2API } from "../../main";

jest.mock('../../../http/http', () => {
    return {
        CoreHttp: jest.fn().mockImplementation(() => {
            return {
                callAPI: jest.fn().mockResolvedValue({success: true}),
            }
        })
    }
});

describe('Inventory', () => {
    let inventory;
    let http;
    let api;

    beforeEach(() => {
        const memstore = new Memstore();
        http = new CoreHttp();
        api = new GW2API(http);
        api.setStorage(memstore);

        inventory = new Inventory(api);
    });

    it('should get some items', async () => {
        const res = await inventory.getItems();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/items');

        expect(res.success).toBeTruthy();
    });

    it('should get items by id', async () => {
        const res = await inventory.getItems([1, 22]);

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/items?ids=1%2C22');

        expect(res.success).toBeTruthy();
    });
})