import { CoreHttp } from "../../../http/http";
import { Memstore } from "../../../storage/memstore";
import { GW2API } from "../../main";
import { Characters } from "./characters";

jest.mock('../../../http/http', () => {
    return {
        CoreHttp: jest.fn().mockImplementation(() => {
            return {
                callAPI: jest.fn().mockResolvedValue({success: true}),
            }
        })
    }
});

describe('Account', () => {
    let chars;
    let http;
    let api;

    beforeEach(() => {
        const memstore = new Memstore();
        http = new CoreHttp();
        api = new GW2API(http);
        api.setStorage(memstore);

        chars = new Characters(api);
    });

    it('should get characters', async () => {
        const res = await chars.get();

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/characters');

        expect(res.success).toBeTruthy();
    });

    it('should get a single character', async () => {
        const res = await chars.get('Daginus');

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/characters/Daginus');

        expect(res.success).toBeTruthy();
    });

    it('should escape character names', async () => {
        const res = await chars.get('Test Character');

        expect(http.callAPI).toHaveBeenCalled();
        expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/characters/Test%20Character');

        expect(res.success).toBeTruthy();
    });
})