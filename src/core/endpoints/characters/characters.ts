import { Endpoint } from "../endpoint";

export class Characters extends Endpoint {

    /**
     * Loads the characters associated with the assigned API token.
     *
     * Requires authenticaion
     *
     * @param {string} characterName
     *  <optional> Get details on a particular character.
     *
     * @return Promise
     */
    public async get(name?: string): Promise<any> {
        let endpoint = '/characters';

        if (typeof name !== 'undefined') {
          endpoint += '/' + encodeURIComponent(name);
        }

        return this.core.callAPI(endpoint);
    }
}