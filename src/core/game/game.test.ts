import { CoreHttp } from "../../http/http";
import { Memstore } from "../../storage/memstore";
import { GW2API } from "../main";
import { Game } from "./game";

jest.mock('../../http/http', () => {
    return {
        CoreHttp: jest.fn().mockImplementation(() => {
            return {
                callAPI: jest.fn().mockResolvedValue({ success: true }),
            }
        })
    }
});

describe('Game', () => {
    let game: Game;
    let http;
    let api;

    beforeEach(() => {
        const memstore = new Memstore();
        http = new CoreHttp();
        api = new GW2API(http);

        api.setStorage(memstore);
        game = new Game(api);
    });

    describe('getCurrencies', () => {
        it('should call /currencies endpoint correctly with single id', async () => {
            const currencyId = 1;
            const res = await game.getCurrencies(currencyId);


            expect(http.callAPI).toHaveBeenCalled();
            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/currencies/1');


        });

        it('should call /currencies endpoint correctly with an array of ids', async () => {
            const currencyIds = [1, 2, 3];
            const res = await game.getCurrencies(currencyIds);
            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/currencies?ids=1%2C2%2C3');

        });

        it('should call /currencies endpoint correctly without ids', async () => {
            const res = await game.getCurrencies();

            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/currencies');

        });
    });

    describe('getMasteries', () => {
        it('should fetch masteries with singular id', async () => {
            const masteryId = 10;
            const res = await game.getMasteries(masteryId);


            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/masteries/10');

        });

        it('should fetch masteries with an array of ids', async () => {
            const masteryIds = [10, 20, 30];
            const res = await game.getMasteries(masteryIds);

            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/masteries?ids=10%2C20%2C30');

        });
    });

    describe('getCommerceExchange', () => {
        it('should call commerce/exchange for gems correctly', async () => {
            const type = 'gems';
            const quantity = 100;
            const res = await game.getCommerceExchange(type, quantity);

            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/commerce/exchange/gems?quantity=100');

        });

        it('should call commerce/exchange for coins correctly', async () => {
            const type = 'coin';
            const quantity = 10000;
            await game.getCommerceExchange(type, quantity);
            
            expect(http.callAPI.mock.calls[0][0].toString()).toEqual('https://api.guildwars2.com/v2/commerce/exchange/coins?quantity=10000');
        });
    });
});

