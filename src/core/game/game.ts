import { Endpoint } from "../endpoints/endpoint";

export class Game extends Endpoint {
  /**
   * Gets currencies. If no ids are passed, this will return an array of all
   * possible material ids.
   * @param  {int|array} currencyIds
   *   <optional> Either an int currencyId or an array of currenciyIds
   * @return Promise
   */
  public async getCurrencies(currencyIds?: number[] | number): Promise<any> {
    return this.core.getOneOrMany("/currencies", currencyIds, false);
  }

  /**
   * Loads Masteries
   *
   * @param {Array<number>} masteryIds
   *
   * @returns {Promise}
   */
  public async getMasteries(masteryIds?: number | number[]): Promise<any> {
    return this.core.getOneOrMany("/masteries", masteryIds, false);
  }

  /**
   * Loads Finishers
   *
   * @param {Array<number>} finisherIds
   *
   * @returns {Promise}
   */
  public async getFinishers(finisherIds?: number[] | number): Promise<any> {
    return this.core.getOneOrMany("/finishers", finisherIds, false);
  }

  /**
   * Gets the world bosses list.
   *
   * @param {number|number[]} ids
   */
  public async getWorldBosses(ids?: number[] | number): Promise<any> {
    return this.core.getOneOrMany("worldbosses", ids, false);
  }

  /**
   * Gets commerce listings. If no item ids are passed, it will return
   * a list of all possible ids.
   *
   * @param  {Int|Array} itemIds
   *   Either an Int or Array of items
   * @return {Promise}
   */
  public async getCommerceListings(itemIds: number[] | number): Promise<any> {
    return this.core.getOneOrMany("/commerce/listings", itemIds, false);
  }

  /**
   * Returns the current gem buy and sell prices.
   *
   * Quantity _must_ be higher than needed to buy a single coin or gem.
   *
   * @param {String} gemOrCoin
   *   The string 'gem' for gold cost to buy gems.
   *   'coin' for gem price for coins.
   * @param {Int} quantity
   *   The number of coins or gems to exchange (this is a required parameter).
   * @return {Promise}
   */
  public async getCommerceExchange(gemOrCoin: "gems" | "coin", quantity: number): Promise<any> {
    const second = gemOrCoin === "gems" ? "gems" : "coins";
    return this.core.callAPI("/commerce/exchange/" + second, { quantity: quantity }, false);
  }
}
