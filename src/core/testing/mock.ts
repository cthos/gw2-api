
/**
 * Mocks the http class to return real-looking responses.
 * 
 * The Response formats were taken from the wiki but:
 * @todo Use real API calls to get the mocks
 */
export class HttpMock {
  public getCallApiMock(): any {
    return jest.fn().mockImplementation((url: URL, options): Promise<any> => {
      const href = url.href;
      if (href.includes('account/bank')) {
        return Promise.resolve(this.getBankResponse());
      } else if (href.includes('/items')) {
        return Promise.resolve(this.getItemResponse());
      } else if (href.includes('account/dyes')) {
        return Promise.resolve(this.getDyesMock());
      } else if (href.includes('colors')) {
        return Promise.resolve(this.getColorMock());
      } else if (href.includes('account/minis')) {
        return Promise.resolve(this.getAccountMinisMock());
      } else if (href.includes('minis')) {
        return Promise.resolve(this.getMinisMock());
      } else if (href.includes('account/wallet')) {
        return Promise.resolve(this.getWallet());
      } else if (href.includes('currencies')) {
        return Promise.resolve(this.getCurrencies());
      } else if (href.includes('account/masteries')) {
        return Promise.resolve(this.getAccountMasteries());
      } else if (href.includes('masteries')) {
        return Promise.resolve(this.getMasteries());
      } else if (href.includes('account/finishers')) {
        return Promise.resolve(this.getAccountFinishers());
      } else if (href.includes('finishers')) {
        return Promise.resolve(this.getFinishers());
      }

      return Promise.resolve({ success: true });
    });
  }

  private getBankResponse(): any[] {
    return [
      {
        "id": 19675,
        "count": 80
      },
      null,
      null,
      {
        "id": 46760,
        "count": 1,
        "upgrades": [
          24580
        ],
        "infusions": [
          37129
        ]
      },
      null,
      {
        "id": 14451,
        "count": 1,
        "skin": 5330
      }
    ];
  }

  private getItemResponse(): any {
    return [{
      "name": "Omnomberry Bar",
      "type": "Consumable",
      "level": 80,
      "rarity": "Fine",
      "vendor_value": 33,
      "id": 12452,
      "chat_link": "[&AgGkMAAA]",
      "icon": "https://render.guildwars2.com/file/6BD5B65FBC6ED450219EC86DD570E59F4DA3791F/433643.png",
      "details": {
        "type": "Food"
      }
    },
    {
      "name": "Testberry Bar",
      "type": "Consumable",
      "level": 80,
      "rarity": "Fine",
      "vendor_value": 33,
      "flags": [
        "NoSell"
      ],
      "restrictions": [],
      "id": 19675,
      "details": {
        "type": "Food"
      }
    },
    {
      "name": "Delicious Dye",
      "type": "Consumable",
      "level": 80,
      "rarity": "Fine",
      "vendor_value": 33,
      "id": 3,
      "chat_link": "[&AgGkMAAA]",
      "icon": "https://render.guildwars2.com/file/6BD5B65FBC6ED450219EC86DD570E59F4DA3791F/433643.png",
      "details": {
        "type": "Dye"
      }
    }];
  }

  private getColorMock() {
    return [
      {
        "id": 3,
        "name": "Sky",
        "base_rgb": [128, 26, 26],
        "cloth": {
          "brightness": 22,
          "contrast": 1.25,
          "hue": 196,
          "saturation": 0.742188,
          "lightness": 1.32813,
          "rgb": [54, 130, 160]
        }
      }];
  }

  private getDyesMock() {
    return [
      3,
      4,
      5,
      6
    ];
  }

  private getMinisMock() {
    return {
      "id": 1,
      "name": "Mini-Rytlock",
      "icon": "https://render.guildwars2.com/file/795ED1B945A29EC3E3066797DF57FFB25ABAA631/340551.png",
      "order": 1,
      "item_id": 21047
    };
  }

  private getAccountMinisMock() {
    return [
      1,
      65,
      67,
      80,
      86,
      93,
      100
    ]
  }

  private getWallet() {
    return [
      {
        "id": 1,
        "value": 100001
      },
      {
        "id": 5,
        "value": 301
      }
    ]
  }

  private getCurrencies() {
    return [
      {
        "id": 1,
        "name": "Coin",
        "description": "The primary currency of Tyria. Spent at vendors throughout the world.",
        "order": 10,
        "icon": "https://render.guildwars2.com/file/98457F504BA2FAC8457F532C4B30EDC23929ACF9/619316.png"
      }
    ];
  }

  private getMasteries() {
    return [{
      "id": 1,
      "name": "Exalted Lore",
      "requirement": "Journey to Auric Basin to unlock the Exalted Lore Mastery track.",
      "order": 2,
      "background": "https://render.guildwars2.com/file/4E09B60E16E6A7404B0638A00D0C6A02F7294308/1228720.png",
      "region": "Maguuma",
      "levels": [
        {
          "name": "Exalted Markings",
          "description": "Gain the knowledge to read Exalted markings. You can now decipher their words and gain access to secrets of their civilization.",
          "instruction": "You can now interact with Exalted artifacts found in Auric Basin and the greater Maguuma Jungle.",
          "icon": "https://render.guildwars2.com/file/7372DCB5085D75F672B50CB8F3577373B8F90468/1228654.png",
          "point_cost": 1,
          "exp_cost": 508000
        },
        {
          "name": "Exalted Gathering",
          "description": "Learn from the Exalted how to gather rare materials around the Maguuma Jungle. Learn to mine up Auric Slivers from ore nodes in Auric Basin.",
          "instruction": "When mining ore while in Auric Basin, you will gain Auric Slivers in addition to normal materials. You also have a chance to mine special items from ore nodes all around the Heart of Maguuma.",
          "icon": "https://render.guildwars2.com/file/324312530AA1A427AB0951D0BFBC92C4B69774D6/1228653.png",
          "point_cost": 8,
          "exp_cost": 3302000
        }
      ]
    }];
  }

  private getAccountMasteries() {
    return [
      {
        "id": 1,
        "level": 4
      },
      {
        "id": 2,
        "level": 5
      },
      {
        "id": 12,
        "level": 3
      },
      {
        "id": 13,
        "level": 2
      }
    ];
  }

  private getAccountFinishers() {
    return [
      {
        "id": 1,
        "permanent": true
      },
      {
        "id": 15,
        "permanent": false,
        "quantity": 5
      }
    ];
  }

  private getFinishers() {
    return [{
      "id": 1,
      "unlock_details": "<c=@reminder>Unlock this PvP rank finisher by earning rank points and increasing your PvP rank.</c>",
      "order": 18,
      "icon": "https://render.guildwars2.com/file/807516C20D08B908946167EADD57980163EECA4E/620101.png",
      "name": "Rabbit Rank Finisher"
    }];
  }
}