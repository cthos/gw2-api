let path = require("path");
let webpack = require("webpack");

module.exports = {
  entry: {
    "gw2-api": "./src/index.ts",
    "gw2-api.min": "./src/index.ts",
  },
  mode: "production",
  output: {
    path: path.resolve(__dirname, "_bundles"),
    filename: "[name].js",
    libraryTarget: "umd",
    library: "GW2API",
    umdNamedDefine: true,
  },
  externals: {
    request: "request",
  },
  externalsPresets: { node: true },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    fallback: {
      util: require.resolve("util"),
      buffer: require.resolve("buffer"),
      stream: require.resolve("stream-browserify"),
      zlib: require.resolve("browserify-zlib"),
      url: require.resolve("url"),
    },
  },
  devtool: "source-map",
  optimization: {
    minimize: true,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
      },
    ],
  },
};
