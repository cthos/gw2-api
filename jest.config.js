module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ["test/api.test.js", "lib", "lib-es6"]
};